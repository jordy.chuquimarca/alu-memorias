--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
--
-- Create Date:   16:57:31 01/23/2022
-- Design Name:   
-- Module Name:   C:/Users/Alba/Desktop/XILINX2/ALU_EXPO/TB_ALU_8BITS.vhd
-- Project Name:  ALU_EXPO
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: ALU_8BITS
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;
 
entity TB_ALU_8BITS is
end entity;

architecture tb of TB_ALU_8BITS is
component ALU_8BITS is
  port (
         a_i      : in std_logic_vector(7 downto 0);
         b_i      : in std_logic_vector(7 downto 0);
         opcode_i : in std_logic_vector(1 downto 0);
         d_o      : out std_logic_vector(7 downto 0)
  );
end component;

signal  a_s      : std_logic_vector(7 downto 0);
signal  b_s      : std_logic_vector(7 downto 0);
signal  opcode_s : std_logic_vector(1 downto 0);
signal  d_s      : std_logic_vector(7 downto 0);

begin
  UUT: ALU_8BITS port map(a_s, b_s, opcode_s, d_s);
  a_s <= "00010001", "01100101" after 5 ns;
  b_s <= "00100011", "00000010" after 5 ns, "00111001" after 20 ns;
  opcode_s <= "00", "01" after 5 ns;

end architecture;
